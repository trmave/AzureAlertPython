# Azure Alert Manager & Visualizer

## Descripción

Este proyecto ofrece una solución integral para la gestión y visualización de alertas en Microsoft Azure. Utilizando Python, el script `Extracciondealertas` automatiza la recolección de alertas de diversas suscripciones de Azure, las clasifica por severidad y las almacena para un análisis detallado. Además, el proyecto incluye una herramienta de visualización, `AlertasxRecurso`, que genera gráficos intuitivos, permitiendo a los usuarios identificar rápidamente los recursos más afectados por las alertas.

## Características

- **Automatización de la Recolección de Alertas:** Recopila alertas de múltiples suscripciones de Azure utilizando `Extracciondealertas`.
- **Clasificación y Análisis:** Clasifica las alertas por severidad y otros criterios.
- **Visualización de Datos:** Genera gráficos de barras para visualizar la frecuencia de alertas por recurso con `AlertasxRecurso`.
- **Fácil de Usar:** Interfaz de usuario sencilla para una rápida configuración y operación.

## Instalación

Para instalar y ejecutar este proyecto, sigue estos pasos:

```bash
git clone https://gitlab.com/trmave/AzureAlertPython.git
cd AzureAlertPython
# Asegúrate de tener Python instalado
# Instala las dependencias necesarias
pip install azure-identity azure-mgmt-alertsmanagement matplotlib
python Extracciondealertas.py
#aca eligen si quieren todas sus suscripciones que puedan usar en tu cuenta de azure o de un listado que usteden elijan dentro del archivo,son dos opciones todas o especificas
python AlertasxRecurso.py 
#este les generara un grafico 