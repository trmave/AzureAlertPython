import json
import matplotlib.pyplot as plt
from collections import Counter

# Cargando los datos del archivo JSON
with open('detalles_alertas.json') as file:
    data = json.load(file)

# Extrayendo los recursos y contando las alertas para cada uno
resource_counts = Counter()
for subscription in data['suscripciones']:
    for alert in subscription['alertas']:
        resource = alert['Recurso Objetivo']
        resource_counts[resource] += 1

# Preparando los datos para el gráfico
resources, counts = zip(*resource_counts.items())

# Creando el gráfico
plt.figure(figsize=(20, 12))
plt.barh(resources, counts, color='skyblue')
plt.xlabel('Número de Alertas')
plt.ylabel('Recurso Objetivo')
plt.title('Número de Alertas por Recurso')
plt.tight_layout()

# Código para guardar el gráfico
plt.savefig('alertas_por_recursov2.png')