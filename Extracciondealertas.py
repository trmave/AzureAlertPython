from azure.identity import DefaultAzureCredential
from azure.mgmt.alertsmanagement import AlertsManagementClient
from azure.mgmt.alertsmanagement.models import TimeRange
from azure.mgmt.resource.subscriptions import SubscriptionClient
import azure.core.exceptions
import json

def get_filtered_subscriptions(subscription_client, filter_names=None):
    all_subscriptions = subscription_client.subscriptions.list()
    return {sub.display_name: sub.subscription_id for sub in all_subscriptions if not filter_names or sub.display_name in filter_names}

def agregar_detalles_alerta(alerta, suscripcion_nombre):
    essentials = alerta.properties.essentials
    detalles = {
        "ID de Alerta": alerta.id,
        "Severidad": essentials.severity,
        "Estado de Alerta": essentials.alert_state,
        "Descripción": essentials.additional_properties.get('description', 'No disponible'),
        "Tipo de Señal": essentials.signal_type,
        "Recurso Objetivo": essentials.target_resource,
        "Nombre del Recurso Objetivo": essentials.target_resource_name,
        "Grupo de Recursos Objetivo": essentials.target_resource_group,
        "Tipo de Recurso Objetivo": essentials.target_resource_type,
        "Servicio de Monitoreo": essentials.monitor_service,
        "Regla de Alerta": essentials.alert_rule,
        "Fecha y Hora de Inicio": essentials.start_date_time.isoformat() if essentials.start_date_time else None,
        "Fecha y Hora de Última Modificación": essentials.last_modified_date_time.isoformat() if essentials.last_modified_date_time else None,
        "Hora en que se resolvió la condición de la alerta": essentials.monitor_condition_resolved_date_time.isoformat() if essentials.monitor_condition_resolved_date_time else None,
        "Último Usuario que Modificó": essentials.last_modified_user_name
    }
    detalles_alertas.setdefault(suscripcion_nombre, []).append(detalles)

# Autenticación con Azure
credential = DefaultAzureCredential()
subscription_client = SubscriptionClient(credential)

# Estructuras de datos para almacenar información
resumen_alertas = {}
detalles_alertas = {}
total_alertas_global = 0
severidades_globales = {"Sev0": 0, "Sev1": 0, "Sev2": 0, "Sev3": 0, "Sev4": 0}

# Modo de selección de suscripciones
modo_seleccion = input("Escribe 'todas' para seleccionar todas las suscripciones o 'especificas' para seleccionar algunas: ").lower()
while modo_seleccion not in ['todas', 'especificas']:
    modo_seleccion = input("Entrada inválida. Escribe 'todas' o 'especificas': ").lower()

if modo_seleccion == 'especificas':
    nombres_suscripciones = [
        "Empresa1 - Sistema1",
        "Empresa1 - Producción Europa",
        "Empresa1 - Producción Global",
        "Empresa2 - Producción EMEA",
        "Socio1 - Centro de Operaciones",
        "NubeVerde - Producción",
        "Empresa2 - Producción AMER",
        "Empresa2 - Sistema1 - Producción",
        "Socio1 - Producción"
    ]

    suscripciones_filtradas = get_filtered_subscriptions(subscription_client, nombres_suscripciones)
else:
    suscripciones_filtradas = get_filtered_subscriptions(subscription_client)

for sub_nombre, sub_id in suscripciones_filtradas.items():
    try:
        print(f"Procesando alertas para la suscripción: {sub_nombre}")
        client = AlertsManagementClient(credential, sub_id)
        time_range = TimeRange("1h")
        alerts = client.alerts.get_all(time_range=time_range)

        severidades = {"Sev0": 0, "Sev1": 0, "Sev2": 0, "Sev3": 0, "Sev4": 0}
        for alert in alerts:
            severidad = alert.properties.essentials.severity
            severidades[severidad] = severidades.get(severidad, 0) + 1
            total_alertas_global += 1
            severidades_globales[severidad] = severidades_globales.get(severidad, 0) + 1
            agregar_detalles_alerta(alert, sub_nombre)

        resumen_alertas[sub_nombre] = {
            "Total de alertas": sum(severidades.values()),
            "Resumen por severidad": severidades
        }

    except azure.core.exceptions.HttpResponseError as e:
        print(f"No se pudo procesar las alertas para la suscripción {sub_nombre}: {e.message}")


# Preparar la estructura final para el archivo JSON
estructura_final = {"suscripciones": []}
for suscripcion, alertas in detalles_alertas.items():
    estructura_final["suscripciones"].append({
        "nombre_suscripcion": suscripcion,
        "alertas": alertas
    })

# Serializar y escribir en archivos JSON
with open('resumen_alertas.json', 'w') as file:
    json.dump(resumen_alertas, file, indent=4)

with open('detalles_alertas.json', 'w') as file:
    json.dump(estructura_final, file, indent=4)

with open('total_alertas.json', 'w') as file:
    json.dump({"Total de alertas global": total_alertas_global, "Resumen global por severidad": severidades_globales}, file, indent=4)
